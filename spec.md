SDSN - Simple Distributed Social Network
============

This is an early document that attempts to address practical issues regarding a new protocol, a reference implementation of the protocol and a web user application for a secure distributed social network (DSN). This research aims to leverage the existing web hosting provider's infrastructures to create drop-in DSN functionality, ideally having no configuration involved.

Adoption of DSN's is being blocked by complex software solutions and protocols. SDSN aims to design a social network in the spirit of the open, free web, as accessible to the end-user as a commodity web site hosting service. The protocol is therefore taken to the lowest common base to be accessible to the average to inexperienced system administrators and webmasters. 

SDSN has two main goals: simplicity and security. It does not aim to create new technology nor engineering methods, but to build a DSN that works.

Companies and organizations are welcome to contribute and comment.
Contact antoniomotarodrigues@gmail.com.

The problem
============

The DSN problem includes some of the most challenging issues, including a practical response to the worldwide legacy email system and practical alternatives to the current centralized social networks.

While highly complex centralized social networks exist, they present common problems:

Mindset: based on advertising models geared towards the end-consumer entertainment that model the network scope itself, making it unfeasible for enterprise and professional environments.

Control and ownership: unpredictable changes in terms of service, infrastructure, features and user interfaces with hidden costs for organisations and individuals.

Privacy: industrial and governmental espionage, individual privacy and freedom of thought are growing concerns.

Additionaly, a solution for a DSN may contribute to address several issues:

-  Email is still important in the enterprise but having known problems like spam, unstructured data, heavy maintenance costs and low reliability.

- Systems complexity: heavily redundant communication and data structures.


Current research and solutions
============

Several researchers have been trying to find practical solutions for the DSN problem.

However, there are notable pitfalls that contribute to postpone a widespread solution of DSN at the same level of email adoption:

- Complexity of protocols: most programmers are kept away from participation while complexity expands.

- Complexity of administration in terms of system requirements, software installation procedures and maintenance.

- Complexity of user interfaces: radically changing design and visual elements placement, and feature accumulation beyond the most important and simple goals frequently confuse end-users.

- Complexity of mindset: un-natural distinction between forms of communication (chat, file transfer, email, forum, blog, wall, microblog...) based on tradition, not logic, keep end-users without a clue about how to use systems.

A simple social protocol may help people and organizations communicating while boosting productivity by using near-realtime structured data objects capable of both human and system communication.


Philosophy
============

SDSN Protocol aims to be the simplest protocol for social networking and follows the following principles:

- Take what everybody already knows.

- Use the existing web platform as it is.

- Zero or near-zero cost.

- Keep the protocol, code, and reference user interface extremely simple.

- Optimization for later, create a basic implementation without special requirements.

- Secure communications.


Implementation
============

The installation procedure consists in dropping a small directory tree inside a public html directory without further configuration needs.

For enterprise systems, distributed datastore backends, cache systems, web proxies and other solutions may be implemented. Our prototype implementation focuses on fast testing on commodity cloud web hosting without a database backend nor other systems dependency except a widespread scripting language running via HTTP server.

Furthermore, early implementations must be created in simple, compatible and readable code, understandable by the average skilled developers.


Usage
============

1. Create a "stream". Share the stream key with a single person, a group of people or general public.

2. Place content in the stream.

3. Upon reception of a key through a safe channel, save it to get access to remote stream.


Definitions
============

- SDSN: a web service that maintains a communication node using the SDSN Protocol. An SDSN server may include a web application for end users. An SDSN may host many users. A user may host many streams and use other streams at the same server or at a remote server. A stream may have several posts. A post may have several files attached. A post may have several comments, which are child posts.

- Object: a unit of structured data of a "type". 

- Type: a metadata structure having a universally unique identifier. Types are: "user", "post", "stream", "token". Files, like pictures, videos and other media are attached as an HTTP form input.

- User: an end user that uses SDSN.

- Stream: Data container for chronological content.

- Key: a secret key used to encrypt and decrypt messages. Encryption methodology is not yet defined.

- Post: a message of formatted text with optional file attachments inserted in a stream.


System architecture notes
============

SDSN has no concept of "email, "wall", "chat room", "forum", "group", "blog", "community page", "notes", "album", but tries to capture the common essence of all in the form of a stream post.

A stream accessible by two or more parties is the equivalent of one or more of the forementioned models depending on the content of the posts.

Underlying protocol: HTTP at port 80.

Messaging: HTTP POST to a web resource at the /sdsn directory (http://www.example.com/sdsn). Each request has several HTTP parameters.

Mandatory in each POST:

- opr (operation)
- usr (destination username)
- sid (stream id)

Mandatory in sending objects:

- oid (source object id)
- obj (JSON object data)
- tok (stream token)

Optional file attachments:

- a1 (attached file 1)
- a2 (attached file 2)
- an (attached file n)

Character encoding: all textual communication is UTF-8 encoded.

Data integrity: an sdsn acts as a backup server of posts received. In case of a third-party sdsn data loss, the other sdsns will eventually ping their trusted sdsns and make lost data available again. 

Ownership is based on a internet domain. Ideally a domain owner may rebuild the service in some web hosting facility without direct backup procedures.

Stream tokens are generated for each stream user and must be shared manually between parties. 

Ideally no login forms, no passwords are involved in all the communication processes.

Ideally, a communication should be a single transaction.


Error codes
============

SDSN only replies if the communication was successful with the code "OK" (without quotes) in the HTTP body, without any kind of additional data.

Communication never returns status codes nor any data that may help spam transmission activities.


Web app random notes
============

Note: This section is being substantially reviewed and may change in near future.

- Ideally an SDSN client is a javascript app cached in the user's file system.

- When a user enters a stream, an SDSN gets access to user profile. 

- The app is saved in a user's folder along with token files. If the user wants to post to a stream or read from a stream, he/she drags the key file to the browser window. He/she also drags and drops file icons as attachments and starts writing.

- The app may give free access to host users and streams as in roaming philosophy. Contents are never published. Only those who have keys will see the streams.


Data structures
============

Note: This section is being substantially reviewed and may change in near future.

Data is organized by objects. 

Every object has a type (user | stream | post | token), a globally unique identifier (localserialnumber + random string at least 20 chars), a timestamp (YYYYMMDDHHIISS).

A data structure is transfered between sdsns using JSON serialized data.

SDSN has some basic object types:

user:

- "type": "user"
- "guid": max. 255 characters
- "time": a creation timestamp
- "subdomain": user's sdsn domain
- "name": user first and last name: string of 64 characters
- "username": system user name
- "about": string of 512 characters
- "photo": png image 200x200px base64.encoded
- "token": user token

stream: 

- "type": "stream"
- "guid": max. 255 characters
- "time": a creation timestamp
- "title": 255 characters
- "about": 1024 characters
- "subdomain": stream subdomain
- "user": stream creator username
- "token": stream token 
- "photo": png image 200x200px base64-encoded
- "maxpostsize": maximum post size in KiB

post:

- "type": "post"
- "guid": max. 255 characters
- "time": a creation timestamp
- "username": max. 255 characters
- "postguid": empty | parent post guid if post is a comment
- "subdomain": Recipent sdsn domain name
- "post": string, 512 characters
- "attach": for each file: "mimetype", "filename", "data": base64 string
- "user": a user object if the transaction with the sdsn server is the first of the day.

token:

- "type": "token"
- "guid": max. 255 characters
- "time": a creation timestamp 
- "oguid": the user or stream object that keeps the corresponding token: max. 255 characters
- "token": token data


References
============

Note: This section is being substantially reviewed and may change in near future.

- Ongoing research is influenced by several projects and research documents, including Diaspora, Camlistore, Tent and others.


Contributors
============

- António Rodrigues
- Hopefully others.


Commented by:
============

- Daniel Fonseca 
- Vitor Jesus

-- END --
